from datetime import datetime
from datetime import date as dt
import calendar
from flask import render_template, flash, redirect, url_for, request, g
from flask_login import current_user, login_required
from flask_babel import get_locale
from app import db
from app.main.forms import EditProfileForm, FinanceForm, CategoryForm
from app.models import User, Finance, Category
from app.main import bp
from sqlalchemy import and_, desc


@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
    g.locale = str(get_locale())


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    current_year = dt.today().year
    current_month = dt.today().month
    previous = current_month - 1
    next_page = current_month + 1
    num_days = calendar.monthrange(current_year, current_month)[1]
    start_date = dt(current_year, current_month, 1)
    end_date = dt(current_year, current_month, num_days)
    page = request.args.get('page', 1, type=int)

    items_page = Finance.query.filter(and_(Finance.date >= start_date, Finance.date <= end_date)).order_by(desc(
        Finance.date)).paginate(page, 20, False)
    next_url = url_for('main.index', page=items_page.next_num) \
        if items_page.has_next else None
    prev_url = url_for('main.index', page=items_page.prev_num) \
        if items_page.has_prev else None
    items = Finance.query.filter(and_(Finance.date >= start_date, Finance.date <= end_date)).order_by(
        Finance.date).all()

    income = 0
    outcome = 0

    last_balance = Finance.query.order_by(desc(Finance.id)).first_or_404()
    for i in items:
        if i.floatto == 'plus':
            income = i.summ + income
        elif i.floatto == 'minus':
            outcome = i.summ + outcome

    finance_sum_income = income
    finance_sum_outcome = outcome

    form = FinanceForm()
    if form.validate_on_submit():

        if form.floatto.data == 'plus':
            balance = last_balance.balance + form.summ.data

            finance = Finance(summ=form.summ.data, describe=form.description.data, date=form.date.data,
                              floatto=form.floatto.data, user_id=current_user.id, balance=balance,
                              spent=finance_sum_outcome)
            db.session.add(finance)
            db.session.commit()
            flash('Добавлено в базу!', 'success')
            print('1')
            return redirect(url_for('main.index'))

        else:
            print('2')
            balance = last_balance.balance - form.summ.data

            finance = Finance(summ=form.summ.data, describe=form.description.data, date=form.date.data,
                              floatto=form.floatto.data, user_id=current_user.id, balance=balance,
                              spent=finance_sum_outcome)
            db.session.add(finance)
            db.session.commit()
            flash('Добавлено в базу!', 'success')
            return redirect(url_for('main.index'))

    return render_template('index.html', title='Главная', form=form, finance_summ_income=finance_sum_income,
                           finance_summ_outcome=finance_sum_outcome, finance_summ_rest=last_balance.balance,
                           year=current_year, month=current_month, items=items, items_page=items_page.items,
                           previous=previous, next=next_page, index=True, next_url=next_url, prev_url=prev_url)


@bp.route('/delete_record/<int:id>')
@login_required
def delete_record(id):
    record = Finance.query.filter_by(id=id).first_or_404()
    db.session.delete(record)
    db.session.commit()
    flash('Запись удалена!', 'warning')
    return redirect(url_for('main.index'))


@bp.route('/<get_year>/<get_month>')
@login_required
def month(get_year, get_month):
    get_year = int(get_year)
    get_month = int(get_month)
    num_days = calendar.monthrange(get_year, get_month)[1]
    start_date = dt(get_year, get_month, 1)
    end_date = dt(get_year, get_month, num_days)
    previous = get_month - 1
    next_page = get_month + 1

    page = request.args.get('page', 1, type=int)

    items_page = Finance.query.filter(and_(Finance.date >= start_date, Finance.date <= end_date))\
        .order_by(desc(Finance.date)).paginate(page, 20, False).items
    items = Finance.query.filter(and_(Finance.date >= start_date, Finance.date <= end_date)).all()
    finance_sum = Finance.query.filter(and_(Finance.date >= start_date, Finance.date <= end_date)).all()
    income = 0
    outcome = 0
    for i in finance_sum:
        if i.floatto == 'plus':
            income = i.summ + income
        elif i.floatto == 'minus':
            outcome = i.summ + outcome
    num_days = calendar.monthrange(get_year, previous)[1]
    start_date = dt(get_year, previous, 1)
    end_date = dt(get_year, previous, num_days)
    balance = Finance.query.filter(and_(Finance.date >= start_date, Finance.date <= end_date)).order_by(
        desc(Finance.date)).first()
    if balance:
        if balance.balance is not None:
            balance = balance.balance
        else:
            balance = 0.0
    else:
        balance = 0.0

    finance_sum_income = income
    finance_sum_outcome = outcome
    finance_sum_rest = income - outcome
    return render_template('monthly.html', year=get_year, month=get_month, start_date=start_date, end_date=end_date,
                           next=next_page, previous=previous, items=items, items_page=items_page,
                           finance_summ_outcome=finance_sum_outcome, finance_summ_income=finance_sum_income,
                           finance_summ_rest=finance_sum_rest, rest_from_month_before=balance, index=True)


@bp.route('/detailed', methods=['GET', 'POST'])
@login_required
def detailed():
    current_year = dt.today().year
    current_month = dt.today().month
    previous = current_month - 1
    next_page = current_month + 1
    num_days = calendar.monthrange(current_year, current_month)[1]
    start_date = dt(current_year, current_month, 1)
    end_date = dt(current_year, current_month, num_days)
    items = Finance.query.filter(and_(Finance.date >= start_date,
                                      Finance.date <= end_date)).order_by(Finance.date).all()

    def detailed_search(word):
        query = Finance.query.filter(and_(Finance.date >= start_date, Finance.date <= end_date,
                                          Finance.describe == word)).order_by(Finance.date).all()
        sum = 0

        for i in query:
            if i.floatto == 'minus':
                sum = i.summ + sum
        return sum

    d = {}
    for x in items:
        detailed_sum = detailed_search(x.describe)
        if detailed_sum > 0:

            k = x.describe
            d.update({k: detailed_sum})

    return render_template('/detailed.html', d=d, title='Сумма по категориям')


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        current_user.start_balance = form.balance.data
        db.session.commit()
        flash('Ваши изменения успешно сохранены')
        return redirect(url_for('main.user', username=current_user.username))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
        form.balance.data = current_user.start_balance
    return render_template('edit_profile.html', title='Редактирование профиля', profile=True,
                           form=form)


@bp.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash("Пользователь %s не существует." % username)
        return redirect(url_for('main.index'))
    if user == current_user:
        flash('Вы не можете подписаться на свои обновления!')
        return redirect(url_for('main.user', username=username))
    current_user.follow(user)
    db.session.commit()
    flash('Вы подписались на пользователя %s !' % username)
    return redirect(url_for('main.user', username=username))


@bp.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('Пользователь %s не существует.', username)
        return redirect(url_for('main.index'))
    if user == current_user:
        flash('Вы не можете отписаться от своих обновлений!')
        return redirect(url_for('main.user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash('Вы отписались от пользователя %s.' % username)
    return redirect(url_for('main.user', username=username))


@bp.route('/settings', methods=['POST', 'GET'])
@login_required
def settings():
    category = CategoryForm()

    if category.validate_on_submit():
        categories = Category(name=category.name.data, user_id=current_user.id)
        db.session.add(categories)
        db.session.commit()
        flash('Добавлена категория %s.' % category.name.data)
        return redirect(url_for('main.settings'))
    else:
        categories = Category.query.all()
    return render_template('settings.html', title='Настройки', settings=True, category=category, categories=categories)


@bp.route('/user/<username>')
@login_required
def user(username):
    year = dt.today().year
    month = dt.today().month
    previous = month - 1
    next = month + 1
    num_days = calendar.monthrange(year, month)[1]
    start_date = dt(year, month, 1)
    end_date = dt(year, month, num_days)

    user = User.query.filter_by(username=username).first_or_404()
    if current_user.id == user.id:
        finance_summ = Finance.query.filter(and_(Finance.date >= start_date, Finance.date <= end_date,
                                                 Finance.user_id == current_user.id)).all()
    else:
        finance_summ = Finance.query.filter(and_(Finance.date >= start_date, Finance.date <= end_date,
                                                 Finance.user_id == user.id)).all()
    income = 0
    outcome = 0
    for i in finance_summ:
        if i.floatto == 'plus':
            income = i.summ + income
        elif i.floatto == 'minus':
            outcome = i.summ + outcome
    return render_template('user.html', user=user, profile=True, title=username, income=income, outcome=outcome)
