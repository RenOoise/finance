from flask import request
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, FloatField, SelectField, DateField
from wtforms.validators import ValidationError, DataRequired, Length
from app.models import User
from datetime import date


class EditProfileForm(FlaskForm):
    username = StringField('Имя пользователя', validators=[DataRequired()])
    first_name = StringField('Имя', validators=[DataRequired()])
    last_name = StringField('Фамилия')
    about_me = TextAreaField('Обо мне',
                             validators=[Length(min=0, max=140)])
    balance = FloatField('Стартовая сумма', validators=[DataRequired()], render_kw={"placeholder": "Стартовая сумма"})
    submit = SubmitField('Сохранить')

    def __init__(self, original_username, *args, **kwargs):
        super(EditProfileForm, self).__init__(*args, **kwargs)
        self.original_username = original_username

    def validate_username(self, username):
        if username.data != self.original_username:
            user = User.query.filter_by(username=self.username.data).first()
            if user is not None:
                raise ValidationError('Пожалуйста, введите другое имя.')


class PostForm(FlaskForm):
    post = TextAreaField('Скажите чо-нить', validators=[DataRequired()])
    submit = SubmitField('Отправить')


class FinanceForm(FlaskForm):
    summ = FloatField(u'Сумма', validators=[DataRequired()], render_kw={"placeholder": "Сумма"})
    description = SelectField(u'Категория', validators=[DataRequired()],
                              choices=[('Продукты', 'Продукты'),
                                       ('Разное', 'Разное'),
                                       ('Проезд', 'Проезд'),
                                       ('Зарплата', 'Зарплата'),
                                       ('Кредит', 'Кредит'),
                                       ('Кварплата', 'Кварплата'),
                                       ('Здоровье', 'Здоровье'),
                                       ('Долг', 'Долг'),
                                       ('Рабочие расходы', 'Рабочие расходы'),
                                       ('Прочее', 'Прочее')])

    date = DateField(u'Дата', validators=[DataRequired()], format="%d.%m.%Y", default=date.today)
    floatto = SelectField(u'Тип операции', validators=[DataRequired()], choices=[('minus', 'Расход'), ('plus', 'Доход')])
    submit = SubmitField(u'Добавить')


class CategoryForm(FlaskForm):
    name = StringField('Категория', validators=[DataRequired()])
    submit = SubmitField('Добавить')


class AddStartBalance(FlaskForm):
    balance = FloatField('Стартовая сумма', validators=[DataRequired()])
    add = SubmitField(u'Добавить')
